extends Panel

signal win(winner)

export (String) var current_turn

var board

func reset():
	board = [[null, null, null], [null, null, null], [null, null, null]]
	for c in $Tiles.get_children():
		c.reset()

func _ready():
	reset()
	for c in $Tiles.get_children():
		c.connect("on_click", self, "parse_click")

func switch_turn():
	current_turn = "X" if current_turn == "O" else "O"

func parse_click(tile, row, col):
	tile.set_content(current_turn)
	board[row][col] = current_turn
	switch_turn()
	check_board()

func check_draw():
	for row in board:
		for col in row:
			if col == null:
				return
	switch_turn()
	reset()

func check_winner():
	if board[0][0] != null and \
		(board[0][0] == board[0][1] and board[0][0] == board[0][2] \
		or board[0][0] == board[1][0] and board[0][0] == board[2][0] \
		or board[0][0] == board[1][1] and board[0][0] == board[2][2]):
			return board[0][0]
	
	if board[1][1] != null and \
		(board[1][1] == board[0][1] and board[1][1] == board[2][1] \
		or board[1][1] == board[1][0] and board[1][1] == board[1][2] \
		or board[1][1] == board[0][2] and board[1][1] == board[2][0]):
			return board[1][1]
	
	if board[2][2] != null and \
		(board[2][2] == board[2][1] and board[2][2] == board[2][0] \
		or board[2][2] == board[1][2] and board[2][2] == board[0][2]):
			return board[2][2]

func check_board():
	var winner = check_winner()
	
	if winner != null:
		emit_signal("win", winner)
		reset()
	else:
		check_draw()