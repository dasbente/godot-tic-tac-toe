extends CanvasLayer

signal reset_game()

var scoreO = 0
var scoreX = 0

func reset():
	scoreO = 0
	scoreX = 0
	update_ui()
	emit_signal("reset_game")

func update_ui():
	$OScore.text = str(scoreO)
	$XScore.text = str(scoreX)

func score(player):
	if player == "X":
		scoreX += 1
	elif player == "O":
		scoreO += 1
	update_ui()