extends Node

func _ready():
	reset()

func reset():
	$PlayBoard.reset()

func process_win(winner):
	$CanvasLayer.score(winner)