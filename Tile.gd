extends TextureButton

signal on_click(tile, row, col)

export (int) var row_index
export (int) var col_index

func reset():
	$XTex.hide()
	$OTex.hide()
	disabled = false

func _ready():
	reset()

func set_content(cont):
	disabled = true
	if cont == "X":
		$XTex.show()
	elif cont == "O":
		$OTex.show()

func _on_Tile_pressed():
	emit_signal("on_click", self, row_index, col_index)
